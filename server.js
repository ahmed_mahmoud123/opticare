var express=require('express');
var mongoose=require('mongoose');
var patient=require('./server/routes/patient');
var fs=require("fs");
const config=JSON.parse(fs.readFileSync(`${__dirname}/env.json`));
var app=express();
app.use(express.static('dist'));
mongoose.connect(config.development.DB_URL);


app.use(function (req, res, next) {
    res.header('transfer-encoding', 'chunked');
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
});

// Loading Routes
fs.readdirSync("./server/routes/").forEach(file=>{
    let route=require(`./server/routes/${file}`);
    app.use(`/${route.route_name}`,route);
});



app.get('/*',(req,resp)=>{
  resp.sendFile(__dirname+"/dist/index.html");
});



app.listen(process.env.PORT || 4000);
