import { FormArray,FormControl} from '@angular/forms';

export class LensExam{
  examination_date:string="-";
  r_base_curve:string="-";
  r_diameter:string="-";
  r_sph:string="-";
  r_cyl:string="-";
  r_axis:string="-";
  r_add:string="-";
  r_addType:string="-";
  r_dva:string="-";
  r_dbinoc:string="-";
  r_nva:string="-";
  r_nBinco:string="-";
  r_clBrand:string="-";
  r_clName:string="-";
  r_clColor:string="-";
  wearingMode:string="1";
  days:string="-";
  hours:string="-";
  solution:string="-";
  // Left
  l_base_curve:string="-";
  l_diameter:string="-";
  l_sph:string="-";
  l_cyl:string="-";
  l_axis:string="-";
  l_add:string="-";
  l_addType:string="-";
  l_dva:string="-";
  l_dbinoc:string="-";
  l_nva:string="-";
  l_nBinco:string="-";
  l_clBrand:string="-";
  l_clName:string="-";
  l_clColor:string="-";
  ex_label:string="CHECK UP";
  patient_id:string="-";
  optometrist:string="-";
  ex_type:string="lens";
  internal_notes:string="-";
  advice_given:string="-";
  reExamination_after:string="1";
  recall_details:FormArray;
  reExamination_advised:string;

  constructor(){
    let now=new Date();
    this.examination_date=`${now.getDate()}/${now.getMonth()+1}/${now.getFullYear()}`;
    now.setMonth(now.getMonth()+1);
    this.reExamination_advised=`${now.getDate()}/${now.getMonth()+1}/${now.getFullYear()}`;;
    let options=[];
    [false,false,false,false,false].forEach(method=>{
      options.push(new FormControl(method));
    });
    this.recall_details=new FormArray(options);
  }

  get_recall_details(){
    return [
      "No Ocular Pathology",
      "Ocular Pathology being managed in practice",
      "No Prescription was Required",
      "An unchanged prescription was issued",
      "A new or changed prescription was issued"
    ]
  }

  get_reExaminate_after(){
    return [1,2,3,4,5,6,7,8,9,10,11,12,18,24];
  }

  addMonths(num:number) {
    var d = new Date();
    var years = Math.floor(num / 12);
    var months = num - (years * 12);
    if (years) d.setFullYear(d.getFullYear() + years);
    if (months) d.setMonth(d.getMonth() + months);
    return `${d.getDate()}/${d.getMonth()+1}/${d.getFullYear()}`;;
  }

}
