export class ContactLensInventory{
  item_code:string="-";
  item_code_str:string="-";
  cl_brand:string="-";
  cl_name:string="-";
  supplier:string="-";
  modality:string="1";
  base_curve:string="";
  sph:string="-";
  cyl:string="-";
  axis:string="-";
  near_add:string="-";
  inter_add:string="-"
  quantity:string="-";
  reorder_level:string="-";
  cost:string="-";
  selling_price:string="-";
}
