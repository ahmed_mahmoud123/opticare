export class FrameInventory{
  item_code:string="-";
  item_code_str:string="";
  brand:string="-";
  supplier:string="";
  model_name:string="-";
  model_number:string="-";
  colour:string="-";
  size:string="-";
  quantity:string="-";
  gender:string="male";
  frame_type:string="1";
  materials:string="-";
  reorder_level:string="-";
  cost:string="-";
  spring_hinge:string="yes";
  selling_price:string="-";
}
