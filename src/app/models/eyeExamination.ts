import { FormArray,FormControl} from '@angular/forms';

export class EyeExamination{
  ex_label:string="";
  patient_id:string="-";
  optometrist:string="-";
  examination_date:string;
  r_vision:string="-";
  r_sph:string="-";
  r_cyl:string="-";
  r_axis:string="-";
  r_mono_pd:string="-";
  r_dva:string="-";
  pd:string="-";
  r_mono_npd:string="-";
  near_pd:string="-";
  l_vision:string="-";
  l_sph:string="-";
  l_cyl:string="-";
  l_axis:string="-";
  l_mono_pd:string="-";
  l_mono_npd:string="-";
  r_near_add:string="-";
  r_inter_add:string="-";
  l_near_add:string="-";
  l_inter_add:string="-";
  r_hprism_d:string="-";
  r_hprism_n:string="-";
  r_hbase_d:string="-";
  r_vprism_d:string="-";
  r_vbase_d:string="-";
  r_bvd:string="-";
  l_hprism_d:string="-";
  l_hbase_d:string="-";
  l_vprism_d:string="-";
  l_vbase_d:string="-";
  l_bvd:string="-";
  r_mf_add:string="-";
  l_mf_add:string="-";
  l_hprism_n:string="-";
  l_hbase_n:string="-";
  r_hbase_n:string="-";
  r_vprism_n:string="-";
  r_vbase_n:string="-";
  l_vprism_n:string="-";
  l_vbase_n:string="-";
  r_nva:string="-";
  l_nva:string="-";
  l_dva:string="-";
  ex_type:String="eye";
  internal_notes:string="";
  advice_given:string="";
  product_recommendation:FormArray;
  prisms_for:string="-";
  reExamination_after:string="1";
  reExamination_advised:string;
  recall_details:FormArray;

  constructor(){
    let now=new Date();
    this.examination_date=`${now.getDate()}/${now.getMonth()+1}/${now.getFullYear()}`;
    now.setMonth(now.getMonth()+1);
    this.reExamination_advised=`${now.getDate()}/${now.getMonth()+1}/${now.getFullYear()}`;;
    let options=[];
    [false,false,false,false,false].forEach(method=>{
      options.push(new FormControl(method));
    });
    this.recall_details=new FormArray(options);
    options=[];
    [false,false,false,false,false].forEach(method=>{
      options.push(new FormControl(method));
    });
    this.product_recommendation=new FormArray(options);

  }

  get_recall_details(){
    return [
      "No Ocular Pathology",
      "Ocular Pathology being managed in practice",
      "No Prescription was Required",
      "An unchanged prescription was issued",
      "A new or changed prescription was issued"
    ]
  }

  get_product_recommendation(){
    return [
      "Near",
      "Dist",
      "Progressives",
      "Vari Focals",
      "Contact Lens"
    ]
  }

  get_reExaminate_after(){
    return [1,2,3,4,5,6,7,8,9,10,11,12,18,24];
  }

  addMonths(num:number) {
    var d = new Date();
    var years = Math.floor(num / 12);
    var months = num - (years * 12);
    if (years) d.setFullYear(d.getFullYear() + years);
    if (months) d.setMonth(d.getMonth() + months);
    return `${d.getDate()}/${d.getMonth()+1}/${d.getFullYear()}`;;
  }
}
