export class LensInventory{
  item_code:string="-";
  item_code_str:string="-";

  lens_type:string="1";
  lens_name:string="-";
  supplier:string="-";
  material:string="-";
  base_curve:string="";
  sph:string="-";
  cyl:string="-";
  axis:string="-";
  near_add:string="-";
  inter_add:string="1";
  index:string="-";
  quantity:string="-";
  reorder_level:string="-";
  cost:string="-";
  selling_price:string="-";
}
