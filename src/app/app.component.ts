import { Component } from '@angular/core';
import { Router,ActivatedRoute,NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  current_route:string;
  constructor(private router:Router){
    router.events.subscribe((event:any) => {
      if(event instanceof NavigationStart) {
        this.current_route = event.url
      }
    });

  }
}
