import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class InventoryService {
  headers:Headers;
  api_url:string;
  constructor(public http: Http) {

    this.api_url="http://198.12.156.161:4000/inventory";
    //this.api_url="http://localhost:4000/inventory";
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
  }

  list(page=1){
    return this.http.get(`${this.api_url}/index/${page}`,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }


  advanced_search(searchObject={},page=1){
    return this.http.post(`${this.api_url}/search`,
      JSON.stringify({query:searchObject,page:page})
      ,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  add_inventory(inventory){
    return this.http.post(`${this.api_url}/new`,JSON.stringify(inventory),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  update_inventory(inventory,id){
    return this.http.post(`${this.api_url}/update`,JSON.stringify({inventory:inventory,id:id}),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  get_inventory(item_code){
    return this.http.get(`${this.api_url}/get/${item_code}`,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  get_max_item_code(){
    return this.http.get(`${this.api_url}/max_item_code`,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }


  get_max_item_code_formUI(control1,control2){
    this.get_max_item_code().then(rs=>{
      let item_code_str="";
      let item_code;
      if(!rs)
        {item_code=1;}
      else{
        item_code=rs.item_code;
        item_code++;
      }
      for (let i = item_code.toString().length; i < 5; i++) {
          item_code_str+="0";
      }
      item_code_str+=item_code;
      control1.setValue(item_code_str);
      control2.setValue(item_code);
    });
  }
}
