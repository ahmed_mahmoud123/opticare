import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ExamService {
  headers:Headers;
  api_url:string;
  constructor(public http: Http) {

    this.api_url="http://198.12.156.161:4000/exam";
    //this.api_url="http://localhost:4000/exam";
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
  }

  add_exam(exam){
    return this.http.post(`${this.api_url}/new`,JSON.stringify(exam),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  update_exam(exam,id){
    return this.http.post(`${this.api_url}/update`,JSON.stringify({exam:exam,id:id}),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }
  get_exam(exam_id){
    return this.http.get(`${this.api_url}/get/${exam_id}`,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  get_patient_exams(patient_id){
    return this.http.get(`${this.api_url}/get_patient_exams/${patient_id}`,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }



}
