import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class PatientsService {
  headers:Headers;
  api_url:string;
  constructor(public http: Http) {
    this.api_url="http://198.12.156.161:4000/patient/";
    //this.api_url="http://localhost:4000/patient/";
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
  }

  add_patient(patient){
    return this.http.post(`${this.api_url}/new`,JSON.stringify(patient),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }


  update_patient(patient){
    return this.http.post(`${this.api_url}/update`,JSON.stringify(patient),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  search(type,query,page){
    let req={
      query:query,
      wild_search:type,
      page:page
    }
    return this.http.post(`${this.api_url}/search`,JSON.stringify(req),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  delete(ids){
    ids=ids.map(patient=>{
      return patient._id;
    });
    ids=JSON.stringify(ids);
    return this.http.get(`${this.api_url}/delete/${ids}`,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }



  index(page){
    return this.http.get(`${this.api_url}/index/${page}`,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }


  get_patient_details(patient_id){
    return this.http.get(`${this.api_url}/details/${patient_id}`,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }


}
