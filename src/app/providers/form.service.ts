import { Injectable } from '@angular/core';
import { Form,FormBuilder,FormArray,FormControl,FormGroup,Validators } from '@angular/forms';


@Injectable()
export class FormService {

  constructor(private formBuilder:FormBuilder) {

  }

  build_form(model:any){
    let formContollers={};
    for (let field in model) {
        if(model[field] instanceof FormArray)
          formContollers[field]=model[field];
        else
          formContollers[field]=[model[field],Validators.required];
    }
    return this.formBuilder.group(formContollers);
  }

  clear(form){
    for (let i in form.controls) {
        form.controls[i].setValue("-");
    }
  }


}
