import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class GenericService {
  headers:Headers;
  api_url:string;
  constructor(public http: Http) {
    this.api_url="http://198.12.156.161:4000";
    //this.api_url="http://localhost:4000";
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
  }

  post(urn,object){
    return this.http.post(`${this.api_url}/${urn}`,JSON.stringify(object),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  get(urn){
    return this.http.get(`${this.api_url}/${urn}`,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }




}
