import { BrowserModule,Title } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule,Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import {SelectModule} from 'ng2-select';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MenuComponent } from './components/menu/menu.component';
import { PatientRegisterationComponent } from './components/patient-registeration/patient-registeration.component';
import { SearchPatientComponent } from './components/search-patient/search-patient.component';
import { PatientProfileComponent } from './components/patient-profile/patient-profile.component';
import { NewExaminationComponent } from './components/new-examination/new-examination.component';
import { ManageEyeexamComponent } from './components/manage-eyeexam/manage-eyeexam.component';
import { ExamHistoryComponent } from './components/exam-history/exam-history.component';
import { ManageLensExamComponent } from './components/manage-lens-exam/manage-lens-exam.component';
import { AddInventoryComponent } from './components/add-inventory/add-inventory.component';
import { SearchInventoryComponent } from './components/search-inventory/search-inventory.component';
import { FrameInventoryComponent } from './components/frame-inventory/frame-inventory.component';
import { LensInventoryComponent } from './components/lens-inventory/lens-inventory.component';
import { CisInventoryComponent } from './components/cis-inventory/cis-inventory.component';
import { AccessoriesInventoryComponent } from './components/accessories-inventory/accessories-inventory.component';
import { FeedbackComponentComponent } from './components/feedback-component/feedback-component.component';
import { AdvancedSearchComponent } from './components/advanced-search/advanced-search.component';
import { DetailsInventoryComponent } from './components/details-inventory/details-inventory.component';

import { NewOrderComponent } from './components/new-order/new-order.component';
import { LoginComponent } from './components/login/login.component';


import { DynamicPopupComponent } from './popup-component/dynamic-popup.component';


import { FormService } from './providers/form.service';
// Define the routes
const ROUTES:Routes = [
  { path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'patient_register',
    component: PatientRegisterationComponent
  },
  {
    path: 'lenses_exam/:exam_label/:id',
    component: ManageLensExamComponent
  },
  {
    path: 'eye_exam/:exam_label/:id',
    component: ManageEyeexamComponent
  },
  {
    path: 'exam_history/:user_id',
    component: ExamHistoryComponent
  },
  {
    path: 'new_exam',
    component: NewExaminationComponent
  },
  {
    path: 'patient_profile/:id',
    component: PatientProfileComponent
  },
  {
    path: 'patient_search',
    component: SearchPatientComponent
  },
  {
    path: 'patient_search/:action',
    component: SearchPatientComponent
  }
  ,
  {
    path: 'add_inventory',
    component:AddInventoryComponent
  },
  {
    path: 'search_inventory',
    component: SearchInventoryComponent
  },
  {
    path: 'details_inventory/:id',
    component: DetailsInventoryComponent
  },
  {
    path: 'new_order/:id',
    component: NewOrderComponent
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    PatientRegisterationComponent,
    SearchPatientComponent,
    PatientProfileComponent,
    NewExaminationComponent,
    ManageEyeexamComponent,
    ExamHistoryComponent,
    ManageLensExamComponent,
    AddInventoryComponent,
    SearchInventoryComponent,
    FrameInventoryComponent,
    LensInventoryComponent,
    CisInventoryComponent,
    AccessoriesInventoryComponent,
    FeedbackComponentComponent,
    AdvancedSearchComponent,
    DetailsInventoryComponent,
    NewOrderComponent,
    LoginComponent,
    DynamicPopupComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule.forRoot(ROUTES),
    FormsModule,
    SelectModule,
    TagInputModule,
    BrowserAnimationsModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA],
  providers: [Title,FormService],
  bootstrap: [AppComponent]
})
export class AppModule { }
