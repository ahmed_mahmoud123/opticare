import { Component,Input,Output,EventEmitter } from '@angular/core';
import { GenericService } from '../providers/generic.service';

@Component({
  templateUrl : './dynamic-popup.component.html',
  selector : "dynamic-popup",
  styleUrls:['./dynamic-popup.component.scss'],
  providers:[GenericService]
})

export class DynamicPopupComponent{

  item_name:string;
  hidden:boolean=false;
  constructor(private __genericService:GenericService){

  }
  @Input('config') config:any;
  @Output('saved') saved:any = new EventEmitter();

  save(){
    console.log(this.item_name);
    this.hidden=true;
    this.__genericService.post(this.config.urn,{name:this.item_name}).then(rs=>{
      this.saved.emit({type:this.config.label});
    });
  }
}
