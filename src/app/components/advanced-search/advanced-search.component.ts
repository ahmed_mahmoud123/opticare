import { Component, OnInit , Output , EventEmitter} from '@angular/core';

@Component({
  selector: 'app-advanced-search',
  templateUrl: './advanced-search.component.html',
  styleUrls: ['./advanced-search.component.css']
})
export class AdvancedSearchComponent implements OnInit {
  tags:Array<any>=[];
  inventories:any={};
  keys:Array<number>=[13,188];
  @Output() searchData: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  search(){
    this.searchData.emit({tags:this.tags,inventories:this.inventories});
  }
}
