import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-new-examination',
  templateUrl: './new-examination.component.html',
  styleUrls: ['./new-examination.component.css']
})
export class NewExaminationComponent implements OnInit {

  exam_type:string="eye_exam";
  exam_label:string="STANDARD";
  patientId:string="";
  exam_labels:any=[{value:"STANDARD",text:"Standard"},{value:"RECHECK",text:"Recheck"}];
  constructor(private title:Title) {
    this.title.setTitle("New Exam");
  }

  ngOnInit() {
  }

  type_change(){
    if(this.exam_type=="eye_exam"){
      this.exam_label="STANDARD";
      this.exam_labels=[{value:"STANDARD",text:"Standard"},{value:"RECHECK",text:"Recheck"}];
    }else{
      this.exam_label="CHECK UP";
      this.exam_labels=[{value:"CHECK UP",text:"Check Up"},{value:"TEACH",text:"Teach"},{value:"FITTING",text:"Fitting"}];
    }
  }

  setPatientID(id){
    this.patientId=id;
  }

}
