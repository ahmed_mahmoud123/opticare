import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ExamService } from '../../providers/exam.service';
import { Patient } from '../../models/patient';
import { PatientsService } from '../../providers/patients.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-exam-history',
  templateUrl: './exam-history.component.html',
  styleUrls: ['./exam-history.component.css'],
  providers:[PatientsService,ExamService]
})
export class ExamHistoryComponent implements OnInit {
  patient_id:number;
  patient:Patient=new Patient();
  no_exam:boolean=true;
  exam=undefined;
  exams:Array<any>=[];
  constructor(private route: ActivatedRoute,public title:Title,private examService:ExamService) {
  }

  ngOnInit() {
    this.patient_id = this.route.snapshot.params['user_id'];
    this.examService.get_patient_exams(this.patient_id).then(exams=>{
      this.exams=exams;
      if(exams.length){
        this.exams=exams.map(ex=>{
          return {id:ex._id,text:ex.examination_date+"  "+ex.time+" "+ex.ex_label};
        });
        this.no_exam=false;
        this.examService.get_exam(exams[0]._id).then(exam=>{
          this.exam=exam;
        });
      }
    });
  }


  get_patient_details(patient){
    this.patient=patient;
    this.title.setTitle(`Examination History - ${patient.first_name} ${patient.last_name}`);
  }

  exam_date_selected(date){
    console.log(date.id)
    this.examService.get_exam(date.id).then(exam=>{
      this.exam=exam;
    });
  }

}
