import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InventoryService } from '../../providers/inventory.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-details-inventory',
  templateUrl: './details-inventory.component.html',
  styleUrls: ['./details-inventory.component.css'],
  providers:[InventoryService]

})
export class DetailsInventoryComponent implements OnInit {
  type:string;
  id:string;
  inventory:any;
  constructor(public title:Title,public route: ActivatedRoute,private _inventoryService:InventoryService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.title.setTitle("Inventory Details");
    this._inventoryService.get_inventory(this.id).then(inventory=>{
      if(inventory.length){
        this.inventory=inventory.pop();
        switch(this.inventory.inventory_type){
          case "frames":
            this.type="1";
          break;
          case "sun_glass":
            this.type="2";
          break;
          case "cls_inventory":
            this.type="3";
          break;
          case "lens_inventory":
            this.type="4";
          break;
          case "accessories_inventory":
            this.type="5";
          break;
        }
      }
    });
  }

}
