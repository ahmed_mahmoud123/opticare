import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { PatientsService } from '../../providers/patients.service';
import { Patient } from '../../models/patient';
import { Router,ActivatedRoute } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-search-patient',
  templateUrl: './search-patient.component.html',
  styleUrls: ['./search-patient.component.css'],
  providers:[PatientsService]
})
export class SearchPatientComponent implements OnInit {
  @Input('showTitle') showTitle:boolean=true;
  @Input('accord_title') accord_title:string="Search Patient Details";
  @Input('resultClick') resultClick:string;
  @Output() onPatientSelect: EventEmitter<any> = new EventEmitter<any>();
  search:any={};
  patients:Array<Patient>=[];
  checkAll:boolean=false;
  wild_search:boolean=false;
  total:number=0;
  page=1;
  last_page=0;
  constructor(private router:Router,private activatedRoute:ActivatedRoute,private title:Title,private patientsService:PatientsService) {
    this.index();
  }

  index(page=1){
    this.patientsService.index(page).then(patients=>{
      this.patients=patients.docs;
      this.total=patients.total;
      this.last_page=Math.ceil(this.total/10);
    })
  }
  ngOnInit() {
    $('[tabindex=1]').focus();
    if(this.showTitle){
      this.title.setTitle("Patient Search")
    };
    window.scroll(0, 0);
  }

  doSearch(page=1){
    if(!this.wild_search){
      this.search.wild_search=undefined;
    }else{
      let key=this.search.wild_search;
      this.search={};
      this.search.wild_search=key;
    }
    this.patientsService.search(this.wild_search,this.search,page).then(patients=>{
      this.patients=patients.docs;
      this.total=patients.total;
      this.page=patients.page;
      this.last_page=Math.ceil(this.total/10);
      this.checkAll=false;
    });
  }


  first(event){
    event.preventDefault();
    this.page=1;
    this.doSearch();
  }

  last(event){
    event.preventDefault();
    this.doSearch(this.last_page);
  }

  next(){
    event.preventDefault();
    this.doSearch(++this.page);
  }

  prev(){
    event.preventDefault();
    this.doSearch(--this.page);
  }
  onChange(value,checked){
    this.checkAll=true;
    for (let i = 0; i < this.patients.length; i++) {
        if(value==this.patients[i]._id){
          this.patients[i].checked=checked;
        }
        if(!this.patients[i].checked)
          this.checkAll=false;
    }
  }

  toggleAll(checked){
    this.checkAll=checked;
    for (let i = 0; i < this.patients.length; i++) {
          this.patients[i].checked=checked;
    }
  }

  delete(){
    if(confirm('are you sure ?')){
      let deleted_patients=this.patients.filter(patient=>{
        return patient.checked==true;
      });
      this.patientsService.delete(deleted_patients).then(patients=>{
        this.doSearch();
      })
    }
  }

  select_patient(_id){
    this.onPatientSelect.emit(_id);
  }

  navigate(_id){
    let action = this.activatedRoute.snapshot.params['action'];
    switch(action){
      case "new_order":
        this.router.navigate(['/new_order/'+_id]);
      break;
      default:
        this.router.navigate(['/patient_profile/'+_id]);
    }
  }
}
