import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Form,FormBuilder,FormArray,FormControl,FormGroup,Validators } from '@angular/forms';
import { Patient } from '../../models/patient';
import { PatientsService } from '../../providers/patients.service';
import { ActivatedRoute } from '@angular/router';
import { GenericService } from '../../providers/generic.service';

declare var $: any;

@Component({
  selector: 'app-patient-profile',
  templateUrl: './patient-profile.component.html',
  styleUrls: ['./patient-profile.component.css'],
  providers:[FormBuilder,PatientsService,GenericService]

})
export class PatientProfileComponent implements OnInit {
  popups_arr:Array<any>=[
    {
      id:"city",
      urn:"city/new",
      label:"City"
    },
    {
      id:"district",
      urn:"district/new",
      label:"District"
    }
  ]
  profileForm;
  methodsName:Array<any>=["Phone","Post","Sms","E-mail"];
  p_status_arr_names:Array<any>=["Key Patient","Member of Staff","Staff Relative"];

  patient:Patient=new Patient();
  success:boolean=false;
  error:boolean=false;
  patient_id:number;
  cities:Array<any>=[];
  districts:Array<any>=[];
  constructor(private __genericService:GenericService,private route: ActivatedRoute,public title:Title,private formBuilder:FormBuilder,private patientsService:PatientsService)
  {
    this.patient.p_status_arr=[false,false,false];
    this.populate_form(this.patient);

  }

  update_patient(){
    if(this.profileForm.valid){
      window.scroll(0, 0);
      this.patient=this.profileForm.value;
      this.patient._id=this.patient_id+"";
      this.patientsService.update_patient(this.patient).then(result=>{
        this.success=true;
      },error=>{
        this.error=true;
      }).catch(err=>this.error=true)
    }
  }
  ngOnInit() {
    let self=this;
    $('[tabindex=1]').focus();
    $('.select2').select2({width: "100%",placeholder:"Select Value"});
    $('.select2').change(function(){
      self.profileForm.controls[$(this).attr('formControlName')].setValue($(this).val());
    });
    this.patient_id = this.route.snapshot.params['id'];
    this.patientsService.get_patient_details(this.patient_id).then(patient=>{
      this.populate_form(patient);
      this.title.setTitle(`Patient Profile - ${patient.first_name} ${patient.last_name}`);
    },err=>{
      console.log(JSON.stringify(err));
    }).catch(err=>console.log(JSON.stringify(err)));
    window.scroll(0, 0);
    this.get_data('city',"cities");
    this.get_data('district',"districts");
  }

  populate_form(patient:Patient){
    let formArrayControls=[];
    let p_status_controls=[];

    patient.receive_methods.forEach(method=>{
    formArrayControls.push(new FormControl(method));
    });
    let methods=new FormArray(formArrayControls);
    patient.p_status_arr.forEach(status=>{
      p_status_controls.push(new FormControl(status));
    });
    let p_status=new FormArray(p_status_controls);
    this.profileForm=this.formBuilder.group({
      first_name:[patient.first_name,Validators.required],
      middle_name:[patient.middle_name],
      last_name:[patient.last_name,Validators.required],
      national_id:[patient.national_id],
      title:[patient.title,Validators.required],
      dob:[patient.dob,Validators.required],
      gender:[patient.gender],
      country:[patient.country],
      address:[patient.address],
      state:[patient.state],
      city:[patient.city],
      district:[patient.district],
      postal_code:[patient.postal_code],
      home_phone:[patient.home_phone],
      work_phone:[patient.work_phone],
      mobile:[patient.mobile,Validators.required],
      email:[patient.email,Validators.required],
      insured:[patient.insured],
      insurance_company_name:[patient.insurance_company_name],
      insurance_number:[patient.insurance_number],
      receive_methods:methods,
      info:[patient.info],
      p_status:[patient.p_status],
      p_status_arr:p_status
    });
  }

  update(event){
    switch(event.type){
      case "City":
        this.get_data('city',"cities");
      case "District":
        this.get_data('district',"districts");
      break;
    }
  }

  get_data(field,arr){
    this.__genericService.get(`${field}/get`).then(data=>{
      if(data.length){
        this[arr]=data;
        this.profileForm.controls[field].setValue(data[data.length-1]._id);
      }
    })
  }
}
