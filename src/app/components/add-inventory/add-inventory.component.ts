import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-add-inventory',
  templateUrl: './add-inventory.component.html',
  styleUrls: ['./add-inventory.component.css']
})
export class AddInventoryComponent implements OnInit {
  error:boolean=false;
  success:boolean=false;
  activeTabe:string="1";
  constructor(public title:Title) {
    title.setTitle("Add Inventory");
  }

  ngOnInit() {
  }

  radioChange(event){
    this.activeTabe=event.target.value;
  }

}
