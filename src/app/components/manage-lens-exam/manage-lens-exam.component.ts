import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PatientsService } from '../../providers/patients.service';
import { Title } from '@angular/platform-browser';
import { Form,FormBuilder,FormArray,FormControl,FormGroup,Validators } from '@angular/forms';
import { Patient } from '../../models/patient';
import { LensExam } from '../../models/lensExam';
import { ExamService } from '../../providers/exam.service';

declare var $: any;
@Component({
  selector: 'app-manage-lens-exam',
  templateUrl: './manage-lens-exam.component.html',
  styleUrls: ['./manage-lens-exam.component.css'],
  providers:[FormBuilder,PatientsService,ExamService]
})
export class ManageLensExamComponent implements OnInit {
  @Input('patientId') patient_id:number=0;
  operation:string="added";
  @Input() exam;
  btn_title:string="Save Only";
  @Output() patientDetailBack: EventEmitter<any> = new EventEmitter<any>();
  error:boolean=false;
  success:boolean=false;
  patient:Patient=new Patient();
  lensForm;
  lensExam:LensExam=new LensExam();
  base_curve_arr=[];
  diameter_arr=[];
  sph_values=[];
  cyl_values=["DS"];
  axis_values=[];
  add_arr=[];
  recall_values=this.lensExam.get_recall_details();
  reExaminate_after_arr=this.lensExam.get_reExaminate_after();
  constructor(private examService:ExamService,private formBuilder:FormBuilder,private patientsService:PatientsService,private route: ActivatedRoute,private title:Title) {
    this.title.setTitle("Lens Exam");
    for (let i = 7; i <= 9; i+=.1)
        this.base_curve_arr.push(i.toFixed(1));
    for (let i = 13; i <= 20; i+=.1)
        this.diameter_arr.push(i.toFixed(1));
    for (let i = 30; i >= -30; i-=.25)
        this.sph_values.push(i>0?"+"+i.toFixed(2):i.toFixed(2));
    for (let i = 0; i >= -10; i-=.25)
        this.cyl_values.push(i.toFixed(2)+"");
    for (let i = 10; i <= 180; i++)
        this.axis_values.push(i+"");
    for (let i = .25; i <= 5.5; i+=.25)
        this.add_arr.push("+"+i.toFixed(2));
  }

  ngOnInit() {
    $('[tabindex=1]').focus();
    let self=this;
    $('.select2').select2({
      width:"100%"
    });
    $('.select2').change(function(){
      if(self.lensForm.controls[$(this).attr('formControlName')])
        self.lensForm.controls[$(this).attr('formControlName')].setValue($(this).val());
    });
    let formContollers={};
    for (let field in this.lensExam) {
      if(this.lensExam[field] instanceof FormArray)
        formContollers[field]=this.lensExam[field];
      else
        formContollers[field]=[this.lensExam[field],Validators.required];
    }
    this.lensForm=this.formBuilder.group(formContollers);

    if(!this.patient_id)
      this.patient._id = this.route.snapshot.params['id'];
    else
      this.patient._id=this.patient_id+"";
    this.lensForm.controls['patient_id'].setValue(this.patient._id);
    this.lensExam.ex_label = this.route.snapshot.params['exam_label'];
    this.patientsService.get_patient_details(this.patient._id).then(patient=>{
      this.patient=patient;
      this.patientDetailBack.emit(patient);
    }).catch(err=>console.log(JSON.stringify(err)));
  }

  ngOnChanges(changes: any) {
    if(this.exam){
      this.btn_title="Update";
      let properties=Object.keys(this.exam);
      setTimeout(()=>{
        this.lensExam.ex_label=this.exam.ex_label;
        console.log(this.lensExam.ex_label);
        for (let i = 0; i < properties.length; i++) {
          if(this.lensForm.controls[properties[i]]){
            this.lensForm.controls[properties[i]].setValue(this.exam[properties[i]]);
          }
        }
        $('.select2').trigger('change.select2');
      },100);
    }
  }
  lens_exam(){
    //if(this.examinationForm.valid){
      let promise=undefined;
      let exam=this.lensForm.value;
      exam.ex_label=this.lensExam.ex_label;
      if(this.exam){
        this.operation="updated";
        promise=this.examService.update_exam(exam,this.exam._id);
      }else{
        promise=this.examService.add_exam(exam);
      }
      promise.then((rs)=>{
        this.success=true;
        this.error=false;
        window.scroll(0, 0);
      },(err)=>{
        this.error=true;
        this.success=false;
        window.scroll(0, 0);
      }).catch((err)=>{
        this.error=true;
        this.success=false;
        window.scroll(0, 0);
      });
    //}
  }

  reExamination_change(){
    let period=this.lensForm.controls['reExamination_after'].value;
    this.lensForm.controls['reExamination_advised'].setValue(this.lensExam.addMonths(period));
  }

}
