import { Component, OnInit,Input } from '@angular/core';
import {AccessoriesInventory} from '../../models/accessories_inventory';
import {FormService} from '../../providers/form.service';
import { InventoryService } from '../../providers/inventory.service';
import { GenericService } from '../../providers/generic.service';

declare var $:any;
@Component({
  selector: 'app-accessories-inventory',
  templateUrl: './accessories-inventory.component.html',
  styleUrls: ['./accessories-inventory.component.css'],
  providers:[InventoryService,GenericService]
})
export class AccessoriesInventoryComponent implements OnInit {
  popups_arr:Array<any>=[
    {
      id:"supplier",
      urn:"supplier/new",
      label:"Supplier"
    },
    {
      id:"category",
      urn:"category/new",
      label:"Category"
    }
  ]
  suppliers:Array<any>=[];
  categories:Array<any>=[];
  @Input('inventory') inventory:any;
  msg:string="Inventory added";
  btn_msg="Save";
  accessories_form:any;
  success:boolean=false;
  error:boolean=false;
  inventory_type:string="accessories_inventory";

  constructor(private __genericService:GenericService,private _inventoryService:InventoryService,private _formService:FormService) {
    this.accessories_form=this._formService.build_form(new AccessoriesInventory());
    this._inventoryService.get_max_item_code_formUI(this.accessories_form.controls['item_code_str'],this.accessories_form.controls['item_code']);

  }

  ngOnInit() {
    let controls=this.accessories_form.controls;
    $('select').change(function(){
      if(controls[$(this).attr('formControlName')])
        controls[$(this).attr('formControlName')].setValue($(this).val());
    });
    $('select').select2({
      width:"100%"
    });
    this.get_data('supplier','suppliers');
    this.get_data('category','categories');
  }

  ngOnChanges(changes: any){
    if(this.inventory){
      this.msg="Inventory updated";
      this.btn_msg="Update";
      for (let prop in this.inventory) {
          if(this.accessories_form.controls[prop])
            this.accessories_form.controls[prop].setValue(this.inventory[prop]);
      }
      setTimeout(()=>{$('select').trigger('change');},100);
    }
  }

  save(){
    let inventory=this.accessories_form.value;
    if(this.inventory){
      this._inventoryService.update_inventory(inventory,this.inventory._id).then(result=>{
        this.success=true;
        this.error=false;
      });
    }else{
      inventory.inventory_type=this.inventory_type;
      this._inventoryService.add_inventory(inventory).then(result=>{
        this.success=true;
        this.error=false;
        this._formService.clear(this.accessories_form);
        this._inventoryService.get_max_item_code_formUI(this.accessories_form.controls['item_code_str'],this.accessories_form.controls['item_code']);
      });
    }
  }

  close(){
    this.error=false;
    this.success=false;
  }

  update(event){
      switch(event.type){
        case "Supplier":
          this.get_data('supplier',"suppliers");
          break;
        case "Category":
          this.get_data('category','categories');
          break;
      }
    }

    get_data(field,arr){
      this.__genericService.get(`${field}/get`).then(data=>{
        if(data.length){
          this[arr]=data;
          console.log(data)
          this.accessories_form.controls[field].setValue(data[data.length-1]._id);
        }
      })
    }

}
