import { Component, OnInit,Input,EventEmitter,Output } from '@angular/core';

@Component({
  selector: 'app-feedback-component',
  templateUrl: './feedback-component.component.html',
  styleUrls: ['./feedback-component.component.css']
})
export class FeedbackComponentComponent implements OnInit {

  @Input('msg') msg:string;
  @Input('success') success:boolean=false;
  @Input('error') error:boolean=false;

  @Output() statusChanged: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }


  close(){
    this.statusChanged.emit();
  }

}
