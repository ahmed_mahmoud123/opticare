import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Form,FormBuilder,FormArray,FormControl,FormGroup,Validators } from '@angular/forms';
import { Patient } from '../../models/patient';
import { PatientsService } from '../../providers/patients.service';
import { GenericService } from '../../providers/generic.service';

import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-patient-registeration',
  templateUrl: './patient-registeration.component.html',
  styleUrls: ['./patient-registeration.component.css'],
  providers:[FormBuilder,PatientsService,GenericService]
})
export class PatientRegisterationComponent implements OnInit {
  popups_arr:Array<any>=[
    {
      id:"city",
      urn:"city/new",
      label:"City"
    },
    {
      id:"district",
      urn:"district/new",
      label:"District"
    }
  ]
  registerationForm;
  methodsName:Array<any>=["Phone","Post","Sms","E-mail"];
  patient:Patient=new Patient();
  success:boolean=false;
  error:boolean=false;
  cities:Array<string>=[];
  districts:Array<string>=[];

  constructor(private __genericService:GenericService,private router: Router,public title:Title,private formBuilder:FormBuilder,private patientsService:PatientsService) {
    this.title.setTitle("New Patient");

    let methods=new FormArray([
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true)
    ])
    this.registerationForm=formBuilder.group({
      first_name:['',Validators.required],
      middle_name:[''],
      last_name:['',Validators.required],
      national_id:[''],
      title:['Mr.',Validators.required],
      dob:['',Validators.required],
      gender:['male'],
      country:['kenya'],
      address:[''],
      state:['-'],
      city:[''],
      district:['-'],
      postal_code:[''],
      home_phone:[''],
      work_phone:[''],
      mobile:['',Validators.required],
      email:['',Validators.required],
      insured:['false'],
      insurance_company_name:[],
      insurance_number:[],
      receive_methods:methods,
      info:['']
    });

  }

  ngOnInit() {
    let body = document.getElementsByTagName('body')[0];
    body.style.backgroundColor = '#4276A4';
    let self=this;
    $('[tabindex=1]').focus();
    $('.select2').select2({width: "100%",placeholder:"Select Value"});
    $('.select2').change(function(){
      self.registerationForm.controls[$(this).attr('formControlName')].setValue($(this).val());
    });
    window.scroll(0, 0);
    this.get_data('city',"cities");
    this.get_data('district',"districts");
  }


  add_patient(event){
    if(this.registerationForm.valid){
      window.scroll(0, 0);
      this.patient=this.registerationForm.value;
      this.patientsService.add_patient(this.patient).then(result=>{
        //this.registerationForm.value={};
        this.success=true;
      },err=>{
        this.error=true;
      }).catch(err=>{
        this.error=true;
      });
    }
  }

  update(event){
    switch(event.type){
      case "City":
        this.get_data('city',"cities");
      case "District":
        this.get_data('district',"districts");
      break;
    }
  }

  get_data(field,arr){
    this.__genericService.get(`${field}/get`).then(data=>{
      if(data.length){
        this[arr]=data;
        this.registerationForm.controls[field].setValue(data[data.length-1]._id);
      }
    })
  }


}
