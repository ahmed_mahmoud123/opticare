import { Component, OnInit,Input } from '@angular/core';
import { FormService } from '../../providers/form.service';
import { InventoryService } from '../../providers/inventory.service';
import { FrameInventory } from '../../models/frame_inventory';
import { GenericService } from '../../providers/generic.service';

declare var $:any;

@Component({
  selector: 'app-frame-inventory',
  templateUrl: './frame-inventory.component.html',
  styleUrls: ['./frame-inventory.component.css'],
  providers:[InventoryService,GenericService],
})
export class FrameInventoryComponent implements OnInit {
  popups_arr:Array<any>=[
    {
      id:"supplier",
      urn:"supplier/new",
      label:"Supplier"
    },
    {
      id:"brand",
      urn:"brand/new",
      label:"Brand"
    },
    {
      id:"colour",
      urn:"colour/new",
      label:"Colour"
    }
  ]
  suppliers:Array<any>=[];
  brands:Array<any>=[];
  colours:Array<any>=[];
  msg:string="Inventory added";
  btn_msg="Save";
  @Input('title') title:string;
  @Input('inventory') inventory:any;
  success:boolean=false;
  error:boolean=false;
  item_code_str:string="";
  item_code:number;
  inventory_form:any;
  inventory_type:string;
  constructor(private __genericService:GenericService,private _inventoryService:InventoryService,private _formService:FormService) {
    this.inventory_form=_formService.build_form(new FrameInventory());
  }


  ngOnChanges(changes: any){
    if(this.title==="1"){
      this.title="Frames";
      this.inventory_type="frames";
    }else if(this.title==="2"){
      this.title="Sun Glass";
      this.inventory_type="sun_glass";
    }

    if(this.inventory){
      this.msg="Inventory updated";
      this.btn_msg="Update";
      for (let prop in this.inventory) {
          if(this.inventory_form.controls[prop])
            this.inventory_form.controls[prop].setValue(this.inventory[prop]);
      }
    }
    setTimeout(()=>{$('select').trigger('change');},100);
  }

  ngOnInit() {
    let controls=this.inventory_form.controls;
    $('select').change(function(){
      if(controls[$(this).attr('formControlName')])
        controls[$(this).attr('formControlName')].setValue($(this).val());
    });
    $('select').select2({
      width:"100%"
    });
    if(!this.inventory)
      this._inventoryService.get_max_item_code_formUI(this.inventory_form.controls['item_code_str'],this.inventory_form.controls['item_code']);
    this.get_data('supplier',"suppliers");
    this.get_data('brand',"brands");
    this.get_data('colour',"colours");

  }

  save(){
    let inventory=this.inventory_form.value;
    if(this.inventory){
      this._inventoryService.update_inventory(inventory,this.inventory._id).then(result=>{
        this.success=true;
        this.error=false;
      });
    }else{
      inventory.inventory_type=this.inventory_type;
      this._inventoryService.add_inventory(inventory).then(result=>{
        this.success=true;
        this.error=false;
        this._formService.clear(this.inventory_form);
        this._inventoryService.get_max_item_code_formUI(this.inventory_form.controls['item_code_str'],this.inventory_form.controls['item_code']);
      });
    }
  }

  close(){
    this.success=false;
    this.error=false;
  }

  update(event){
    switch(event.type){
      case "Supplier":
        this.get_data('supplier',"suppliers");
        break;
      case "Brand":
        this.get_data('brand',"brands");
        break;
      case "Colour":
        this.get_data('colour',"colours");
      break;
    }
  }

  get_data(field,arr){
    this.__genericService.get(`${field}/get`).then(data=>{
      if(data.length){
        this[arr]=data;
        console.log(data)
        this.inventory_form.controls[field].setValue(data[data.length-1]._id);
      }
    })
  }

}
