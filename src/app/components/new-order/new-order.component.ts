import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { PatientsService } from '../../providers/patients.service';

@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.css'],
  providers:[PatientsService]
})
export class NewOrderComponent implements OnInit {
  patient_id:string;
  patient_name:string;
  constructor(private activatedRoute:ActivatedRoute,private title:Title,private patientsService:PatientsService) {
  }

  ngOnInit() {
    this.patient_id = this.activatedRoute.snapshot.params['id'];
    this.patientsService.get_patient_details(this.patient_id).then(patient=>{
      this.title.setTitle(`New Order - ${patient.first_name} ${patient.last_name}`);
      this.patient_name=`${patient.first_name} ${patient.last_name}`;
    },err=>{
      console.log(JSON.stringify(err));
    }).catch(err=>console.log(JSON.stringify(err)));
    window.scroll(0, 0);
  }

}
