import { Component,Input,Output,OnInit,EventEmitter } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Form,FormBuilder,FormArray,FormControl,FormGroup,Validators } from '@angular/forms';
import { Patient } from '../../models/patient';
import { EyeExamination } from '../../models/eyeExamination';

import { ActivatedRoute } from '@angular/router';
import { PatientsService } from '../../providers/patients.service';
import { ExamService } from '../../providers/exam.service';

declare var $: any;

@Component({
  selector: 'app-manage-eyeexam',
  templateUrl: './manage-eyeexam.component.html',
  styleUrls: ['./manage-eyeexam.component.css'],
  providers:[FormBuilder,PatientsService,ExamService]
})

export class ManageEyeexamComponent implements OnInit {
  patient:Patient=new Patient();
  operation:string="added";
  @Input('patientId') patient_id:number=0;
  @Input() exam;
  btn_title:string="Save Only";
  @Output() patientDetailBack: EventEmitter<any> = new EventEmitter<any>();
  error:boolean=false;
  success:boolean=false;
  exam_label:string;
  sph_values:Array<string>=[];
  cyl_values:Array<string>=["DS"];
  axis_values:Array<string>=[];
  mono_pd_values:Array<string>=[];
  mono_npd_values:Array<string>=[];
  tbl4_values:Array<string>=[];
  examinationForm;
  eyeExamination:EyeExamination=new EyeExamination();
  recall_values=this.eyeExamination.get_recall_details();
  product_recommendation=this.eyeExamination.get_product_recommendation();
  reExaminate_after_arr=this.eyeExamination.get_reExaminate_after();
  constructor(private examService:ExamService,private route: ActivatedRoute,private title:Title,private formBuilder:FormBuilder,private patientsService:PatientsService) {
    for (let i = 30; i >= -30; i-=.25)
        this.sph_values.push(i>0?"+"+i.toFixed(2):i.toFixed(2)+"");
    for (let i = 0; i >= -10; i-=.25)
        this.cyl_values.push(i.toFixed(2)+"");
    for (let i = 10; i <= 180; i++)
        this.axis_values.push(i+"");
    for (let i = 20; i <= 40; i+=.5)
        this.mono_pd_values.push(i.toFixed(2)+"");
    for (let i = 20; i <= 40; i+=.25)
        this.mono_npd_values.push(i.toFixed(2)+"");
    for (let i = .25; i <= 5.5; i+=.25)
        this.tbl4_values.push("+"+i.toFixed(2));

    this.title.setTitle("Eye Exam");
  }

  ngOnInit() {

    $('[tabindex=1]').focus();
    let self=this;
    $('.select2').select2({
      width:"100%"
    });
    let formContollers={};
    for (let field in this.eyeExamination) {
        if(this.eyeExamination[field] instanceof FormArray)
          formContollers[field]=this.eyeExamination[field];
        else
          formContollers[field]=[this.eyeExamination[field],Validators.required];
    }
    this.examinationForm=this.formBuilder.group(formContollers);
    $('.select2').change(function(){
      if(self.examinationForm.controls[$(this).attr('formControlName')])
        self.examinationForm.controls[$(this).attr('formControlName')].setValue($(this).val());
      let form=self.examinationForm.value;
      self.examinationForm.controls['pd'].setValue(Number(form.l_mono_pd)+Number(form.r_mono_pd));
      self.examinationForm.controls['near_pd'].setValue(Number(form.l_mono_npd)+Number(form.r_mono_npd));
    });
    if(!this.patient_id)
      this.patient._id = this.route.snapshot.params['id'];
    else
      this.patient._id=this.patient_id+"";
    this.examinationForm.controls['patient_id'].setValue(this.patient._id);
    this.exam_label = this.route.snapshot.params['exam_label'];
    this.patientsService.get_patient_details(this.patient._id).then(patient=>{
      this.patient=patient;
      this.patientDetailBack.emit(patient);
    }).catch(err=>console.log(JSON.stringify(err)));
  }

  ngOnChanges(changes: any) {
    if(this.exam){
      this.btn_title="Update";
      let properties=Object.keys(this.exam);
      setTimeout(()=>{
        this.exam_label=this.exam.ex_label;
        for (let i = 0; i < properties.length; i++) {
          if(this.examinationForm.controls[properties[i]]){
            this.examinationForm.controls[properties[i]].setValue(this.exam[properties[i]]);
          }
        }
        $('.select2').trigger('change.select2');
      },100);
    }
  }

  examination(){
    //if(this.examinationForm.valid){
      let promise=undefined;
      let exam=this.examinationForm.value;
      exam.ex_label=this.exam_label
      if(this.exam){
        this.operation="updated";
        promise=this.examService.update_exam(exam,this.exam._id);
      }else{
        promise=this.examService.add_exam(exam);
      }
      promise.then((rs)=>{
        this.success=true;
        this.error=false;
        window.scroll(0, 0);
      },(err)=>{
        this.error=true;
        this.success=false;
        window.scroll(0, 0);
      }).catch((err)=>{
        this.error=true;
        this.success=false;
        window.scroll(0, 0);
      });
    //}
  }



  reExamination_change(){
    let period=this.examinationForm.controls['reExamination_after'].value;
    this.examinationForm.controls['reExamination_advised'].setValue(this.eyeExamination.addMonths(period));
  }

}
