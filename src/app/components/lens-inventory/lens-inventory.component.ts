import { Component, OnInit,Input } from '@angular/core';
import {LensInventory} from '../../models/lens_inventory';
import {FormService} from '../../providers/form.service';
import { InventoryService } from '../../providers/inventory.service';
import { GenericService } from '../../providers/generic.service';

declare var $:any;
@Component({
  selector: 'app-lens-inventory',
  templateUrl: './lens-inventory.component.html',
  styleUrls: ['./lens-inventory.component.css'],
  providers:[InventoryService,GenericService]

})
export class LensInventoryComponent implements OnInit {
  popups_arr:Array<any>=[
    {
      id:"supplier",
      urn:"supplier/new",
      label:"Supplier"
    },
    {
      id:"lens_name",
      urn:"lens_name/new",
      label:"Lens Name"
    }
  ]
  @Input('inventory') inventory:any;
  msg:string="Inventory added";
  btn_msg="Save";
  sph_values:Array<string>=[];
  cyl_values:Array<string>=["DS"];
  tbl4_values:Array<string>=[];
  axis_values:Array<string>=[];

  lens_form:any;
  suppliers:Array<any>=[];
  lens_names:Array<any>=[];
  success:boolean=false;
  error:boolean=false;
  inventory_type:string="lens_inventory";
  constructor(private __genericService:GenericService,private _inventoryService:InventoryService,private _formService:FormService) {
    for (let i = 30; i >= -30; i-=.25)
        this.sph_values.push(i>0?"+"+i.toFixed(2):i.toFixed(2)+"");
    for (let i = 0; i >= -10; i-=.25)
        this.cyl_values.push(i.toFixed(2)+"");
    for (let i = 10; i <= 180; i++)
        this.axis_values.push(i+"");
    for (let i = .25; i <= 5.5; i+=.25)
        this.tbl4_values.push("+"+i.toFixed(2));

    this.lens_form=this._formService.build_form(new LensInventory());
    this._inventoryService.get_max_item_code_formUI(this.lens_form.controls['item_code_str'],this.lens_form.controls['item_code']);

  }

  ngOnInit() {
    let controls=this.lens_form.controls;
    $('select').change(function(){
      if(controls[$(this).attr('formControlName')])
        controls[$(this).attr('formControlName')].setValue($(this).val());
    });
    $('select').select2({
      width:"100%"
    });
    this.get_data('supplier','suppliers');
    this.get_data('lens_name','lens_names');

  }
  ngOnChanges(changes: any){
    if(this.inventory){
      this.msg="Inventory updated";
      this.btn_msg="Update";
      for (let prop in this.inventory) {
          if(this.lens_form.controls[prop])
            this.lens_form.controls[prop].setValue(this.inventory[prop]);
      }
    }
    setTimeout(()=>{$('select').trigger('change');},100);
  }

  save(){
    let inventory=this.lens_form.value;
    if(this.inventory){
      this._inventoryService.update_inventory(inventory,this.inventory._id).then(result=>{
        this.success=true;
        this.error=false;
      });
    }else{
      inventory.inventory_type=this.inventory_type;
      this._inventoryService.add_inventory(inventory).then(result=>{
        this.success=true;
        this.error=false;
        this._formService.clear(this.lens_form);
        this._inventoryService.get_max_item_code_formUI(this.lens_form.controls['item_code_str'],this.lens_form.controls['item_code']);
      });
    }
  }

  close(){
    this.error=false;
    this.success=false;
  }
update(event){
    switch(event.type){
      case "Supplier":
        this.get_data('supplier',"suppliers");
        break;
      case "Lens Name":
        this.get_data('lens_name',"lens_names");
        break;
    }
  }

  get_data(field,arr){
    this.__genericService.get(`${field}/get`).then(data=>{
      if(data.length){
        this[arr]=data;
        console.log(data)
        this.lens_form.controls[field].setValue(data[data.length-1]._id);
      }
    })
  }

}
