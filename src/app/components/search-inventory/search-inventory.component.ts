import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { InventoryService } from '../../providers/inventory.service';

@Component({
  selector: 'app-search-inventory',
  templateUrl: './search-inventory.component.html',
  styleUrls: ['./search-inventory.component.css'],
  providers:[InventoryService]
})
export class SearchInventoryComponent implements OnInit {
  tags:Array<any>=[];
  tags_1:Array<any>=[];
  all_tags:Array<any>=["None"];
  inventories:Array<any>=[];
  total:number=0;
  page=1;
  last_page=0;
  item_code:string;
  searchMethod:string="index";
  constructor(private _inventoryService:InventoryService,public title:Title) {
    title.setTitle("Inventory Search");
    this[this.searchMethod]();
  }

  index(page=1){
    this._inventoryService.list(page).then(inventories=>{
      this.inventories=inventories.docs;
      this.total=inventories.total;
      this.last_page=Math.ceil(this.total/10);
    })
  }

  first(event){
    event.preventDefault();
    this.page=1;
    this[this.searchMethod]();
  }

  last(event){
    event.preventDefault();
    this.page=this.last_page;
    this[this.searchMethod](this.last_page);
  }

  next(){
    event.preventDefault();
    this[this.searchMethod](++this.page);
  }

  prev(){
    event.preventDefault();
    this[this.searchMethod](--this.page);
  }

  search_by_item_code(event=undefined){
    this.all_tags=[`Item Code: ${this.item_code}`];
    this.inventories=[];
    this._inventoryService.get_inventory(this.item_code).then(inventory=>{
      this.inventories=inventory;
      this.total=0;
    });
  }

  search(data){
    this.item_code="";
    this.tags=[];
    this.tags_1=data.tags;
    for (let property in data.inventories) {
        if(data.inventories[property]){
          this.tags.push(property);
        }
    }
    this.all_tags=this.tags.concat(this.tags_1);
    this.advanced_search();
  }

  advanced_search(){
    this.searchMethod="advanced_search";
    this.tags=this.all_tags.filter(tag=>{
      console.log(typeof tag)
      return typeof tag === "string";
    })
    this.tags_1=this.all_tags.filter(tag=>{
      console.log(typeof tag)
      return typeof tag === "object";
    }).map(tag=> {return tag.value});

    console.log(this.tags_1);
    console.log(this.tags);
    let searchObj={inventories:this.tags,tags:this.tags_1};
    this._inventoryService.advanced_search(searchObj,this.page).then(inventories=>{
      this.inventories=inventories.docs;
      this.total=inventories.total;
      this.last_page=Math.ceil(this.total/10);
    });
    if(this.all_tags.length === 0)
      this.all_tags=["None"];

    if(this.all_tags[0] === "None")
    {
      this.searchMethod="index";
      this.index();
    }
  }



  ngOnInit() {
  }

  input(e){
    if(e.which===13)
      this.search_by_item_code();
  }
}
