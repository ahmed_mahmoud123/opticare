import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private title:Title) {
    let body = document.getElementsByTagName('body')[0];
    body.style.backgroundColor = '#364150';
    title.setTitle("Login");
  }

  ngOnInit() {

  }

}
