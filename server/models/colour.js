var mongoose=require('mongoose');

//var mongoosePaginate = require('mongoose-paginate');

class Colour{

  constructor(){
    this.init();
    this.model=mongoose.model('colours');
  }



  add(data){
    return this.model.collection.insert(data);
  }

  get(){
    return this.model.find({});
  }

  init(){
    var Schema=mongoose.Schema;
    var colour=new Schema({});
    mongoose.model("colours",colour);
  }
}

module.exports=new Colour();
