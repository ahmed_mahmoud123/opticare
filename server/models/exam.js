var mongoose=require('mongoose');

//var mongoosePaginate = require('mongoose-paginate');

class Exam{
  constructor(){
    this.init();
    this.examModel=mongoose.model('exams');
  }



  add_exam(examData){
    let now=new Date();
    for (let property in examData) {
      if(typeof examData[property] === "string")
        examData[property]=examData[property].toUpperCase()
    }
    examData.time=`${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
    examData.datetime=+now;
    console.log(examData);
    return this.examModel.collection.insert(examData);
    //return exam.save();
  }

  update_exam(exam,id){
    console.log(id);
    return this.examModel.collection.update({_id:mongoose.Types.ObjectId(id)},{"$set":exam});
  }
  get_exam(exam_id){
    return this.examModel.findOne({_id:exam_id});
  }

  get_patient_exams(patient_id){
    return this.examModel.find({patient_id:patient_id},{time:true,ex_label:true,examination_date:true}).sort({_id:-1});
  }
  init(){
    var Schema=mongoose.Schema;
    var exam=new Schema({});
    //eyeExam.plugin(mongoosePaginate),
    mongoose.model("exams",exam);
  }
}

module.exports=new Exam();
