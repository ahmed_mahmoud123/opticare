var mongoose=require('mongoose');
var mongoosePaginate = require('mongoose-paginate');


class Inventory{

  get_fields(){
    return [
      "item_code_str",
      "brand",
      "supplier",
      "model_name",
      "model_number",
      "colour",
      "size",
      "quantity",
      "gender",
      "frame_type",
      "materials",
      "reorder_level",
      "cost",
      "spring_hinge",
      "selling_price",
      "lens_type",
      "lens_name",
      "material",
      "base_curve",
      "sph",
      "cyl",
      "axis",
      "near_add",
      "inter_add",
      "index",
      "brand",
      "model_name",
      "modality",
      "category",
      "notes",
    ];
  }
  constructor(){
    this.init();
    this.inventoryModel=mongoose.model('inventories');
  }



  add_inventory(inventoryData){
    let now=new Date();
    /*for (let property in inventoryData) {
      if(typeof examData[property] === "string")
        examData[property]=examData[property].toUpperCase()
    }*/
    inventoryData.time=`${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
    inventoryData.datetime=+now;
    return this.inventoryModel.collection.insert(inventoryData);
  }

  update_inventory(inventory,id){
    return this.inventoryModel.collection.update({_id:mongoose.Types.ObjectId(id)},{"$set":inventory});
  }

  get_inventory(item_code){
    return this.inventoryModel.find({item_code_str:item_code});
  }

  get_max_item_code(){
    return this.inventoryModel.findOne({}).sort({item_code:-1});
  }


  search(query=undefined,page=1){
    if(query)
      return this.inventoryModel.paginate(query,{ sort:{_id:-1},page: page,limit: 10});
    else
      return this.inventoryModel.paginate({},{sort:{_id:-1},page: page,limit: 10});
  }

  init(){
    var Schema=mongoose.Schema;
    var inventory=new Schema({});
    inventory.plugin(mongoosePaginate),
    mongoose.model("inventories",inventory);
  }
}

module.exports=new Inventory();
