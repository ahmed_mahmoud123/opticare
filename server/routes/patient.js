var express=require('express');
var bodyParser=require('body-parser');
let jsonMiddelware=bodyParser.json();
let patientModel=require('../models/patient');

let router=express.Router();


router.post('/new',jsonMiddelware,(req,resp)=>{
  patientModel.add_patient(req.body).then(result=>{
    resp.json(result);
  },err=>{
    console.log(err);
    resp.json(err);
  });
});

router.post('/update',jsonMiddelware,(req,resp)=>{
  patientModel.update_patient(req.body).then(result=>{
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});

router.post('/search',jsonMiddelware,(req,resp)=>{
  patientModel.search_patient(buildQuery(req.body.wild_search,req.body.query),req.body.page).then(patients=>{
    resp.json(patients);
  },err=>console.log(err));
});

router.get('/details/:id/',(req,resp)=>{
  patientModel.patient_details(req.params.id).then(patient=>{
    resp.json(patient);
  },err=>resp.json(err));
});


function buildQuery(wild_search,queryJSON){
  let query={};
  let queryObj=queryJSON;
  console.log(queryObj);
  if(Object.keys(queryObj).length === 0)
    return undefined;
  if(wild_search===false){
    query["$and"]=[];
    for(let cond in queryObj){
      let condition={};
      condition[cond]={'$regex':`.*${queryObj[cond]}.*`, '$options' : 'i'};
      query["$and"].push(condition);
    }
  }else{
    let fields=patientModel.get_fields();
    query["$or"]=[];
    for (let i = 0; i < fields.length; i++) {
      let condition={};
      condition[fields[i]]={'$regex':`.*${queryObj.wild_search}.*`, '$options' : 'i'};
      query["$or"].push(condition);
    }
  }
  return query;
}

router.get('/delete/:ids',(req,resp)=>{
  patientModel.delete_patient(req.params.ids).then(result=>{
    resp.json(result);
  });
});

router.get('/index/:page',(req,resp)=>{
  patientModel.search_patient(undefined,req.params.page).then(patients=>{
    resp.json(patients);
  });
});

router.route_name="patient";
module.exports=router;
