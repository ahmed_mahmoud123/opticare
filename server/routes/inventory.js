var express=require('express');
var bodyParser=require('body-parser');
let jsonMiddelware=bodyParser.json();
let inventoryModel=require('../models/inventory');

let router=express.Router();


router.post('/new',jsonMiddelware,(req,resp)=>{
  inventoryModel.add_inventory(req.body).then(result=>{
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});

router.post('/update',jsonMiddelware,(req,resp)=>{
  inventoryModel.update_inventory(req.body.inventory,req.body.id).then(result=>{
    console.log(result);
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});

router.get('/get/:item_code',(req,resp)=>{
  inventoryModel.get_inventory(req.params.item_code).then(result=>{
    console.log(result);
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});

router.get('/max_item_code',(req,resp)=>{
  inventoryModel.get_max_item_code().exec(function(err,result){
    console.log(result);
    resp.json(result);
  });
});

router.get('/index/:page',(req,resp)=>{
  inventoryModel.search(undefined,req.params.page).then(inventories=>{
    resp.json(inventories);
  });
});


router.post('/search',jsonMiddelware,(req,resp)=>{
  inventoryModel.search(buildQuery(req.body.query),req.body.page).then(inventories=>{
    console.log(inventories);
    resp.json(inventories);
  },err=>console.log(err));
});


function buildQuery(queryObj){
  let query={};

  let tags=queryObj.tags;
  query["$and"]=[];
  if(queryObj.inventories.length > 0)
    query["$and"].push({inventory_type:{'$in':queryObj.inventories}})

  if(tags.length > 0)
  {
    let regex = tags.map(function (e) { return new RegExp(e, "i"); });
    let $orObject={"$or":[]};
    let fields=inventoryModel.get_fields();
    for (let i = 0; i < fields.length; i++) {
      let condition={};
      condition[fields[i]]={"$in": regex };
      $orObject["$or"].push(condition);
    }
    query["$and"].push($orObject);
  }
  return query;
}


router.route_name="inventory";
module.exports=router;
